class RPNCalculator
  def initialize
    @numbers = []
  end

  def push(num)
    @numbers << num
  end

  def value
    @numbers.last
  end

  def plus
    perform_operation(:+)
  end

  def minus
    perform_operation(:-)
  end

  def divide
    perform_operation(:/)
  end

  def times
    perform_operation(:*)
  end

  def tokens(string)
    operations = "+-/*"
    string.split.map do |el|
      if operations.include?(el)
        el.to_sym
      else
        el.to_i
      end
    end
  end

  def evaluate(string)
    tokens = tokens(string)
    tokens.each do |token|
      if el.is_a? Integer
        push(token)
      else
        perform_operation(token)
      end
    end
    
    value
  end

  private

  def perform_operation(op_symbol)
    raise "calculator is empty" if @numbers.length < 2

    operating_nums = @numbers.pop(2)

    case op_symbol
    when :+
      @numbers << operating_nums[0] + operating_nums[-1]
    when :-
      @numbers << operating_nums[0] - operating_nums[-1]
    when :/
      @numbers << operating_nums[0].to_f / operating_nums[-1]
    when :*
      @numbers << operating_nums[0].to_f * operating_nums[-1]
    else
      @numbers += operating_nums
      raise "No such operation #{op_symbol}"
    end
  end

end
